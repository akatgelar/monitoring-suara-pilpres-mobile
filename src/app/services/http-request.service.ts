import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ErrorMessageService } from './error-message.service';
import { SessionService } from './session.service';
import { Session } from 'protractor';

@Injectable()

export class HttpRequestService {



    constructor(
        private http: HttpClient,
        private router: Router,
        private errorMessage: ErrorMessageService,
        public session: SessionService
    ) { }

    public jwt = this.session.getToken();
    public header = { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.jwt };
    public header_login = { 'Content-Type': 'application/json', 'Authorization': ''};

    // post data
    httpLogin(url: string, data: any): Observable<any> {
        return this.http.post( url, data, { headers : new HttpHeaders(this.header_login) })
        .pipe(
            map(result => {
                // console.log('httpPost result');
                // console.log(result);
                return result;
            }),
            catchError(error => {
                // console.log('httpPost error');
                // console.log(error);
                if (String(error.status) === '504') {
                    this.errorMessage.errorserver();
                    return Observable.throw(error);
                } else {
                    try {
                        const error_msg = JSON.parse(error._body);
                        this.errorMessage.openErrorSwal(error_msg['message']);
                        return Observable.throw(error);
                    } catch (err) {
                        const error_msg = error.status + ' ' + error.statusText;
                        this.errorMessage.openErrorSwal(error_msg);
                        return Observable.throw(error);
                    }
                }
            })
        );

    }

    // api get
    httpGetEsri(url: string, data: any): Observable<any> {
        return this.http.get( url, { params: new HttpParams().append('f', data['f']).append('location', data['location']).append('token', data['token']) })
        .pipe(
            map(result => {
                // console.log('httpGet result');
                // console.log(result);
                return result;
            }),
            catchError(error => {
                // console.log('httpGet error');
                // console.log(error);
                if (String(error.status) === '504') {
                    this.errorMessage.errorserver();
                    return Observable.throw(error);
                } else {
                    try {
                        const error_msg = JSON.parse(error._body);
                        this.errorMessage.openErrorSwal(error_msg['message']);
                        return Observable.throw(error);
                    } catch (err) {
                        const error_msg = error.status + ' ' + error.statusText;
                        this.errorMessage.openErrorSwal(error_msg);
                        return Observable.throw(error);
                    }
                }
            })
        );
    }

    // api get
    httpGet(url: string, data: any): Observable<any> {
        return this.http.get( url, { headers : new HttpHeaders(this.header) })
        .pipe(
            map(result => {
                // console.log('httpGet result');
                // console.log(result);
                return result;
            }),
            catchError(error => {
                // console.log('httpGet error');
                // console.log(error);
                if (String(error.status) === '504') {
                    this.errorMessage.errorserver();
                    return Observable.throw(error);
                } else {
                    try {
                        const error_msg = JSON.parse(error._body);
                        this.errorMessage.openErrorSwal(error_msg['message']);
                        return Observable.throw(error);
                    } catch (err) {
                        const error_msg = error.status + ' ' + error.statusText;
                        this.errorMessage.openErrorSwal(error_msg);
                        return Observable.throw(error);
                    }
                }
            })
        );
    }

    // post data
    httpPost(url: string, data: any): Observable<any> {
        // console.log(this.session.getData());
        // console.log(this.jwt);
        return this.http.post( url, data, { headers : new HttpHeaders(this.header) })
        .pipe(
            map(result => {
                // console.log('httpPost result');
                // console.log(result);
                return result;
            }),
            catchError(error => {
                // console.log('httpPost error');
                // console.log(error);
                if (String(error.status) === '504') {
                    this.errorMessage.errorserver();
                    return Observable.throw(error);
                } else {
                    try {
                        const error_msg = JSON.parse(error._body);
                        this.errorMessage.openErrorSwal(error_msg['message']);
                        return Observable.throw(error);
                    } catch (err) {
                        const error_msg = error.status + ' ' + error.statusText;
                        this.errorMessage.openErrorSwal(error_msg);
                        return Observable.throw(error);
                    }
                }
            })
        );
    }

    // update data
    httpPut(url: string, data: any): Observable<any> {
        return this.http.put( url, data, { headers : new HttpHeaders(this.header) })
        .pipe(
            map(result => {
                // console.log('httpPut result');
                // console.log(result);
                return result;
            }),
            catchError(error => {
                // console.log('httpPut error');
                // console.log(error);
                if (String(error.status) === '504') {
                    this.errorMessage.errorserver();
                    return Observable.throw(error);
                } else {
                    try {
                        const error_msg = JSON.parse(error._body);
                        this.errorMessage.openErrorSwal(error_msg['message']);
                        return Observable.throw(error);
                    } catch (err) {
                        const error_msg = error.status + ' ' + error.statusText;
                        this.errorMessage.openErrorSwal(error_msg);
                        return Observable.throw(error);
                    }
                }
            })
        );
    }

    // delete data
    httpDelete(url: string, data: any): Observable<any> {
        return this.http.delete( url, { headers : new HttpHeaders(this.header) })
        .pipe(
            map(result => {
                // console.log('httpDelete result');
                // console.log(result);
                return result;
            }),
            catchError(error => {
                // console.log('httpDelete error');
                // console.log(error);
                if (String(error.status) === '504') {
                    this.errorMessage.errorserver();
                    return Observable.throw(error);
                } else {
                    try {
                        const error_msg = JSON.parse(error._body);
                        this.errorMessage.openErrorSwal(error_msg['message']);
                        return Observable.throw(error);
                    } catch (err) {
                        const error_msg = error.status + ' ' + error.statusText;
                        this.errorMessage.openErrorSwal(error_msg);
                        return Observable.throw(error);
                    }
                }
            })
        );
    }

}
