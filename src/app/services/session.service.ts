import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class SessionService {

  constructor() {
  }

  setToken(token: string) {
    localStorage.setItem('monitoring-pilpres-token', token);
  }

  getToken() {
    return localStorage.getItem('monitoring-pilpres-token');
  }

  setData(data: string) {
    localStorage.setItem('monitoring-pilpres-data', JSON.stringify(data));
  }

  getData() {
    return localStorage.getItem('monitoring-pilpres-data');
  }

  // getRole() {
  //   const data = this.getData();
  //   const result_msg = JSON.parse(data);
  //   try {
  //     let role = result_msg['role']['type'];
  //     role = role.toString().toLowerCase();
  //     role = role.replace('_', '-');
  //     return role;
  //   } catch (error) {
  //     return null;
  //   }
  // }

  getTokenExpired = function() {
    const token = this.getToken();
    const jwtHelper = new JwtHelperService();

    try {
      const date = jwtHelper.getTokenExpirationDate(token);
      return date;
    } catch (error) {
      return null;
    }
  };

  isLoggedIn = function() {
    const token = this.getToken();
    const jwtHelper = new JwtHelperService();

    if (token) {
      const payload = jwtHelper.decodeToken(token);
      const date = jwtHelper.getTokenExpirationDate(token);
      const expired = jwtHelper.isTokenExpired(token);
      // console.log(payload);
      // console.log(date);
      // console.log(expired);

      if (expired) {
        return false;
      } else {
        return true;
      }

    } else {
      return false;
    }
  };

  // checkAccess(url) {
  //   const routing = url.split('/');
  //   const data = this.getData();
  //   const result_msg = JSON.parse(data);
  //   try {
  //     let role = result_msg['role']['type'];
  //     role = role.toString().toLowerCase();
  //     role = role.replace('_', '-');
  //     if (routing[1] === 'error') {
  //       return true;
  //     } else if (routing[1] === 'auth') {
  //       return true;
  //     } else if (routing[1] === role) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   } catch (error) {
  //     return false;
  //   }
  // }

  logIn = function(token, data) {
    this.setData(data);
    this.setToken(token);
    console.log(token);
    console.log(data);
    return this.getToken();
  };

  logOut = function() {
    localStorage.removeItem('monitoring-pilpres-token');
    localStorage.removeItem('monitoring-pilpres-data');
  };

}
