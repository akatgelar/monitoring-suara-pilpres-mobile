import { NgModule, Directive, OnInit, EventEmitter, Output, OnDestroy, Input, ElementRef, Renderer } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginPage } from '../login/login.page';
import { AbsenPage } from '../absen/absen.page';
import { KirimPage } from '../kirim/kirim.page';

@NgModule({
  imports: [
  ],
  declarations: [
    LoginPage,
    AbsenPage,
    KirimPage,
  ],
  exports: [
    LoginPage,
    AbsenPage,
    KirimPage,
  ]
})

export class SharedModule { }
