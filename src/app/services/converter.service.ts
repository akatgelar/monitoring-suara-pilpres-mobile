import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable()
export class ConverterService {

  constructor() {
  }

  formatDate(date: string) {
    if (date) {
      moment.locale('id');
      const new_date = moment(date).format('DD MMMM YYYY hh:mm:ss') ;
      return new_date;
    } else {
      return null;
    }
  }

  formatRupiah(angka: string) {
    if (angka) {
      const prefix = 'Rp. ';
      const number_string = angka.toString().replace(/[^,\d]/g, '');
      const split = number_string.split(',');
      const sisa = split[0].length % 3;
      const ribuan = split[0].substr(sisa).match(/\d{3}/gi);
      let rupiah = split[0].substr(0, sisa);

      // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if (ribuan) {
        const separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] !== undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix === undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
  }


}
