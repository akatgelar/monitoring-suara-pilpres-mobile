import { Component } from '@angular/core';

import { Router, NavigationStart, NavigationEnd, Event as NavigationEvent } from '@angular/router';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { SessionService } from './services/session.service';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    public router: Router,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public geolocation: Geolocation,
    public session: SessionService
  ) {
    this.initializeApp();
    this.firebaseInit();
    this.watchGeoLocation();
    this.checkSession();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  firebaseInit() {
  }

  watchGeoLocation() {
    const watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      // console.log(data.coords.latitude);
      // console.log(data.coords.longitude);
    });
  }

  checkSession() {
    this.router.events.forEach((event: NavigationEvent) => {

      // After Navigation
      // if (event instanceof NavigationEnd) {

      // Berfore Navigation
      if (event instanceof NavigationStart) {
        const url = event.url;
        const routing = url.split('/');

        // console.log('call every change route');
        // console.log('url : ' + url);
        // console.log('routing : ' + routing);
        // console.log('islogin : ' + this.session.isLoggedIn());
        // console.log('tokenExpired : ' + this.session.getTokenExpired());
        // console.log('jwt : ' + this.session.getToken());
        // console.log(JSON.parse(this.session.getData()));

        if ((routing[1] === 'login') || (routing[1] === '')) {
          if (this.session.isLoggedIn()) {
              this.router.navigate(['/tabs/home']);
          } else {
            // this.router.navigate(['/login']);
          }
        } else {
          if (this.session.isLoggedIn()) {
            // this.router.navigate(['/home']);
          } else {
            this.router.navigate(['/login']);
          }
        }
      }
    });
  }

}
