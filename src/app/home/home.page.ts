import { Component, OnInit } from '@angular/core';
import { SessionService } from '../services/session.service';
import { ErrorMessageService } from '../services/error-message.service';
import { HttpRequestService } from '../services/http-request.service';
import { ConverterService } from '../services/converter.service';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { ThrowStmt } from '@angular/compiler';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Event } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {

  str_nama = '';
  str_no_hp = '';
  str_kode_tps = '';
  str_nama_provinsi = '';
  str_nama_kota = '';
  str_nama_kecamatan = '';
  str_nama_kelurahan = '';
  str_nama_tps = '';

  status_absen = false;
  status_kirim = false;

  constructor(
    private router: Router,
    private session: SessionService,
    private httpRequest: HttpRequestService,
    private errorMessage: ErrorMessageService,
    private converter: ConverterService,
    public loadingController: LoadingController,
    public toastController: ToastController) {

      const data = JSON.parse(this.session.getData());
      this.str_nama = data['nama'];
      this.str_no_hp = data['no_hp'];
      this.str_kode_tps = data['kode_tps'];
      this.str_nama_provinsi = this.capitalize(data['nama_provinsi']);
      this.str_nama_kota = this.capitalize(data['nama_kota']);
      this.str_nama_kecamatan = this.capitalize(data['nama_kecamatan']);
      this.str_nama_kelurahan = this.capitalize(data['nama_kelurahan']);
      this.str_nama_tps = data['nama_tps'];

      this.status_absen = data['status_absen'];
      this.status_kirim = data['status_kirim'];
      // console.log(this.status_absen);
      // console.log(this.status_kirim);

      this.session.getData();
      this.session.getToken();
  }

  ngOnInit() {
    this.router.events.subscribe(
      (event: Event) => {
        if (event instanceof NavigationEnd) {

          const data = JSON.parse(this.session.getData());

          this.status_absen = data['status_absen'];
          this.status_kirim = data['status_kirim'];
          // console.log(this.status_absen);
          // console.log(this.status_kirim);
        }
      });
  }

  gotoAbsen() {
    if (this.status_absen !== true) {
      this.router.navigate(['/absen']);
    }
  }

  gotoKirim() {
    if (this.status_kirim !== true) {
      this.router.navigate(['/kirim']);
    }
  }

  capitalize(str) {
    const arr = str.split(' ');
    let res = '';
    arr.forEach(a => {
      const b = a.toLowerCase();
      const c = b.charAt(0).toUpperCase() + b.slice(1);
      res = res + c + ' ';
    });
    res = res.substring(0, res.length - 1);
    return res;
  }
}
