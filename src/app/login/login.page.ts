import { Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from '../services/session.service';
import { ErrorMessageService } from '../services/error-message.service';
import { HttpRequestService } from '../services/http-request.service';
import { ConverterService } from '../services/converter.service';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { async } from '@angular/core/testing';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss']
})
export class LoginPage {
  loginForm: FormGroup;
  // loginUrl = 'http://us-central1-monitoring-suara-pilpres-2019.cloudfunctions.net/login';
  loginUrl = '/api/login';
  // updateUserUrl = 'http://us-central1-monitoring-suara-pilpres-2019.cloudfunctions.net/updateUser';
  updateUserUrl = '/api/updateUser';

  str_latitude = '0.0';
  str_longitude = '0.0';

  constructor(
    public router: Router,
    private session: SessionService,
    private httpRequest: HttpRequestService,
    private errorMessage: ErrorMessageService,
    private converter: ConverterService,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public geolocation: Geolocation) {

    const txt_kode_tps = new FormControl('', [Validators.required]);
    const txt_username = new FormControl('', [Validators.required]);
    const txt_password = new FormControl('', [Validators.required]);

    this.loginForm = new FormGroup({
      kode_tps: txt_kode_tps,
      username: txt_username,
      password: txt_password
    });

    this.getLocation();
  }

  async login() {

    // variable login user
    const loginVal = this.loginForm.value;
    const loginJson = JSON.stringify(loginVal);
    console.log(this.loginUrl);
    console.log(loginJson);

    // loading
    const loading = await this.loadingController.create({
      message: 'Loading...',
      // duration: 2000
    });
    await loading.present();

    // call post login
    this.httpRequest.httpLogin(this.loginUrl, loginJson).subscribe(
      // if ok
      async result => {
        console.log(result);

        if (result['status'] === true) {

          // session set
          this.session.logIn(result['result']['jwt'], result['result']['users']);

          // message
          await loading.dismiss();
          const toast = await this.toastController.create({
            message: result['message'],
            duration: 2000
          });
          await toast.present();

          setTimeout(() => {
            // update user
            this.updateUser(result['result']['users']['kode_tps']);
            // redirect
            this.router.navigate(['/tabs/home']);
          }, 1000);

        } else {
          // message
          await loading.dismiss();
          const toast = await this.toastController.create({
            message: result['message'],
            duration: 2000
          });
          await toast.present();
        }
      },
      // if error
      async error => {
        console.log('error');
        console.log(error);
        // message
        await loading.dismiss();
        const toast = await this.toastController.create({
          message: error.message,
          duration: 2000
        });
        await toast.present();
      }
    );
  }

  async updateUser(kode_tps) {

    // variable update user
    const updateUserVal = {};
    updateUserVal['id'] = kode_tps;
    updateUserVal['latitude'] = this.str_latitude;
    updateUserVal['longitude'] = this.str_longitude;
    const updateUserJson = JSON.stringify(updateUserVal);
    console.log(this.updateUserUrl);
    console.log(updateUserJson);

    // call post
    this.httpRequest.httpPost(this.updateUserUrl, updateUserJson).subscribe(
      // if ok
      async result => {
        console.log(result);
      },
      // if error
      async error => {
        console.log('error');
        console.log(error);
      }
    );
  }

  getLocation() {

    // get geo position
    this.geolocation.getCurrentPosition().then((resp) => {
      this.str_latitude = resp.coords.latitude.toString();
      this.str_longitude = resp.coords.longitude.toString();
      console.log(resp.coords.latitude);
      console.log(resp.coords.longitude);
     }).catch((error) => {
       console.log('Error getting location', error);
     });

  }

}
