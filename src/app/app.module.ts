import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { LoginPage } from './login/login.page';
import { AbsenPage } from './absen/absen.page';
import { KirimPage } from './kirim/kirim.page';
// import { LoginPageModule } from './login/login.module';
// import { AbsenPageModule } from './absen/absen.module';
// import { KirimPageModule } from './kirim/kirim.module';
import { TabsPage } from './tabs/tabs.page';

import { SessionService } from './services/session.service';
import { ErrorMessageService } from './services/error-message.service';
import { HttpRequestService } from './services/http-request.service';
import { ConverterService } from './services/converter.service';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AngularFireModule } from 'angularfire2';
import { AngularFireStorage } from 'angularfire2/storage';
import { AngularFirestore } from 'angularfire2/firestore';

// import { SharedModule } from './services/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    AbsenPage,
    KirimPage
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot({mode: 'ios'}),
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp({
      apiKey: 'AIzaSyByj3yvcMqUWb_8r1icUni28y4KRxr3M-I',
      authDomain: 'monitoring-suara-pilpres-2019.firebaseapp.com',
      databaseURL: 'https://monitoring-suara-pilpres-2019.firebaseio.com',
      projectId: 'monitoring-suara-pilpres-2019',
      storageBucket: 'monitoring-suara-pilpres-2019.appspot.com',
      messagingSenderId: '919294575946'
    }),
    // SharedModule,
    // LoginPageModule,
    // AbsenPageModule,
    // KirimPageModule,
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    StatusBar,
    SplashScreen,
    SessionService,
    HttpRequestService,
    ErrorMessageService,
    ConverterService,
    Geolocation,
    Camera,
    AngularFireStorage,
    AngularFirestore
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
