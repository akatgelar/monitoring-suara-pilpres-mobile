import { Component } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-tabulasi',
  templateUrl: 'tabulasi.page.html',
  styleUrls: ['tabulasi.page.scss']
})



export class TabulasiPage {


  public provinsi: Observable<any[]>;
  public kota: Observable<any[]>;
  public kecamatan: Observable<any[]>;
  public kelurahan: Observable<any[]>;
  public tps: Observable<any[]>;

  public _kode_provinsi: string;
  public _kode_kota: string;
  public _kode_kecamatan: string;
  public _kode_kelurahan: string;
  public _kode_tps: string;

  loadProses: boolean;
  myForm: FormGroup;

  private _data1: number;
  private _data2: number;
  private _dataPersen1: number;
  private _dataPersen2: number;
  private _dataTable: any;
  private _dataTableTotal1: any;
  private _dataTableTotal2: any;
  private _dataTableTotal: any;
  private _dataLoaded_nasional: boolean;
  private _dataLoaded_provinsi: boolean;

  private _kode_nasional: string;
  private _nama_nasional: string;
  private _nasional_latitude: string;
  private _nasional_longitude: string;
  private _validasi: any;
  private _validasi_non: any;

  // Doughnut
  public doughnutChartLabels: String[] = ['Joko Widodo - Ma\'ruf Amin', 'Prabowo Subianto - Sandiaga Uno'];
  public doughnutChartData: number[] = [0, 0];
  public doughnutChartType: String = 'pie';
  public chartColors: any[] = [{ backgroundColor: ['#e74c3c', '#2ecc71'] }];

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(
    public loadingController: LoadingController,
    private router: Router,
    public db: AngularFirestore) {
      // this.myForm.get('provinsi').setValue('');
      // this.myForm.get('kota').setValue('');
      // this.myForm.get('kecamatan').setValue('');
      // this.myForm.get('kelurahan').setValue('');
      // this.myForm.get('tps').setValue('');

      this._kode_provinsi = '';
      this._kode_kota = '';
      this._kode_kecamatan = '';
      this._kode_kelurahan = '';
      this._kode_tps = '';
      this.loadProses = false;

      this.myForm = new FormGroup({
        provinsi: new FormControl(),
        kota: new FormControl(),
        kecamatan: new FormControl(),
        kelurahan: new FormControl(),
        tps: new FormControl()
      });

      this.getProvinsi();
  }

  getProvinsi() {
    this.provinsi = this.db.collection('/test_provinsi', ref => ref.orderBy('nama_provinsi', 'asc')).valueChanges();
  }

  getKota(kode_provinsi) {
    this.kota = this.db.collection('/test_kota', ref => ref.where('kode_provinsi', '==', kode_provinsi).orderBy('nama_kota', 'asc')).valueChanges();
  }

  getKecamatan(kode_kota) {
    this.kecamatan = this.db.collection('/test_kecamatan', ref => ref.where('kode_kota', '==', kode_kota).orderBy('nama_kecamatan', 'asc')).valueChanges();
  }

  getKelurahan(kode_kecamatan) {
    this.kelurahan = this.db.collection('/test_kelurahan', ref => ref.where('kode_kecamatan', '==', kode_kecamatan).orderBy('nama_kelurahan', 'asc')).valueChanges();
  }

  getTPS(kode_kelurahan) {
    this.tps = this.db.collection('/test_tps', ref => ref.where('kode_kelurahan', '==', kode_kelurahan).orderBy('nama_tps', 'asc')).valueChanges();
  }

  onProvinsiChange(kode_provinsi) {
    this._kode_provinsi = kode_provinsi;
    this.kota = null;
    this.kecamatan = null;
    this.kelurahan = null;
    this.tps = null;
    this.myForm.get('kota').setValue('');
    this.myForm.get('kecamatan').setValue('');
    this.myForm.get('kelurahan').setValue('');
    this.myForm.get('tps').setValue('');
    this.getKota(kode_provinsi);
  }

  onKotaChange(kode_kota) {
    this._kode_kota = kode_kota;
    this.kecamatan = null;
    this.kelurahan = null;
    this.tps = null;
    this.myForm.get('kecamatan').setValue('');
    this.myForm.get('kelurahan').setValue('');
    this.myForm.get('tps').setValue('');
    this.getKecamatan(kode_kota);
  }

  onKecamatanChange(kode_kecamatan) {
    this._kode_kecamatan = kode_kecamatan;
    this.kelurahan = null;
    this.tps = null;
    this.myForm.get('kelurahan').setValue('');
    this.myForm.get('tps').setValue('');
    this.getKelurahan(kode_kecamatan);
  }

  onKelurahanChange(kode_kelurahan) {
    this._kode_kelurahan = kode_kelurahan;
    this.tps = null;
    this.myForm.get('tps').setValue('');
    this.getTPS(kode_kelurahan);
  }

  onTPSChange(kode_tps) {
    this._kode_tps = kode_tps;
  }

  async presentLoading() {

    // console.log(this._kode_tps);
    // console.log(this._kode_kelurahan);
    // console.log(this._kode_kecamatan);
    // console.log(this._kode_kota);
    // console.log(this._kode_provinsi);
    let level = '';
    let kode: string;

    if (this._kode_tps !== '') {
      level = 'test_tps';
      kode = this._kode_tps;
    } else if (this._kode_kelurahan !== '') {
      level = 'test_kelurahan';
      kode = this._kode_kelurahan;
    } else if (this._kode_kecamatan !== '') {
      level = 'test_kecamatan';
      kode = this._kode_kecamatan;
    } else if (this._kode_kota !== '') {
      level = 'test_kota';
      kode = this._kode_kota;
    } else if (this._kode_provinsi !== '') {
      level = 'test_provinsi';
      kode = this._kode_provinsi;
    } else {
      level = 'test_nasional';
      kode = '0';
    }

    console.log(level);
    console.log(kode);

    //  get Data
    this.db.collection('/' + level).doc(kode)
    .valueChanges()
    .subscribe(async item =>  {
      console.log(item);

      if (level === 'test_tps') {
        // change chart data
        this._data1 = Number(item['suara_1']);
        this._data2 = Number(item['suara_2']);
        this._dataPersen1 = Number(item['suara_1']) / (Number(item['suara_1']) + Number(item['suara_2'])) * 100;
        this._dataPersen2 = Number(item['suara_2']) / (Number(item['suara_1']) + Number(item['suara_2'])) * 100;

      } else {
        this._validasi = item['validasi'];
        this._validasi_non = item['validasi_non'];

        // change chart data
        this._data1 = this._validasi_non['suara_1'];
        this._data2 = this._validasi_non['suara_2'];
        this._dataPersen1 = this._validasi_non['suara_1'] / (this._validasi_non['suara_1'] + this._validasi_non['suara_2']) * 100;
        this._dataPersen2 = this._validasi_non['suara_2'] / (this._validasi_non['suara_1'] + this._validasi_non['suara_2']) * 100;

      }

      // loading
      const loading = await this.loadingController.create({
        message: 'Loading...',
        duration: 2000
      });
      await loading.present();

      // change chart
      this.doughnutChartData = [this._data1, this._data2];
      await loading.onDidDismiss();

      this.loadProses = true;

    });

  }

  gotoHome() {
    console.log('home');
    this.router.navigate(['/tabs/home']);
  }

  numberPercen(x) {
    return Number(x).toFixed(2);
  }

  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

}
