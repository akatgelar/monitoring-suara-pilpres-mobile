import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabulasiPage } from './tabulasi.page';
import { ChartsModule } from 'ng2-charts';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: TabulasiPage }]),
    ChartsModule
  ],
  declarations: [TabulasiPage]
})
export class TabulasiPageModule {}
