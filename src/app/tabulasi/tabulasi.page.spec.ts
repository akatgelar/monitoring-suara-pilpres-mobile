import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabulasiPage } from './tabulasi.page';

describe('TabulasiPage', () => {
  let component: TabulasiPage;
  let fixture: ComponentFixture<TabulasiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabulasiPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabulasiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
