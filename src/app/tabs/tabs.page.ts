import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(private router: Router) {}

  gotoHome() {
    console.log('home');
    this.router.navigate(['/tabs/home']);
  }
  gotoProfile() {
    console.log('profile');
    this.router.navigate(['/tabs/profile']);
  }
  gotoBerita() {
    console.log('berita');
    this.router.navigate(['/tabs/berita']);
  }
  gotoTabulasi() {
    console.log('tabulasi');
    this.router.navigate(['/tabs/tabulasi']);
  }
}
