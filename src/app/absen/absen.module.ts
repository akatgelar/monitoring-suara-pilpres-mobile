import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// import { AbsenPageRoutingModule } from './absen.router.module';

import { AbsenPage } from './absen.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    // AbsenPageRoutingModule
  ],
  // declarations: [AbsenPage]
})
export class AbsenPageModule {}
