import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbsenPage } from './absen.page';

describe('AbsenPage', () => {
  let component: AbsenPage;
  let fixture: ComponentFixture<AbsenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AbsenPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbsenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
