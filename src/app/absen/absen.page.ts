import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from '../services/session.service';
import { ErrorMessageService } from '../services/error-message.service';
import { HttpRequestService } from '../services/http-request.service';
import { ConverterService } from '../services/converter.service';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-absen',
  templateUrl: 'absen.page.html',
  styleUrls: ['absen.page.scss']
})



export class AbsenPage {
  esriReverseGeocodeUrl = 'https://api.mapbox.com/geocoding/v5/mapbox.places/';
  // esriReverseGeocodeUrl = '/esri/reverseGeocode';
  // updateUserUrl = 'http://us-central1-monitoring-suara-pilpres-2019.cloudfunctions.net/updateUser';
  updateUserUrl = '/api/updateUser';

  str_latitude = '0.0';
  str_longitude = '0.0';
  str_address = '';
  str_datetime = '';

  constructor(
    public router: Router,
    private session: SessionService,
    private httpRequest: HttpRequestService,
    private errorMessage: ErrorMessageService,
    private converter: ConverterService,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public geolocation: Geolocation) {

    this.getLocation();
    console.log(new Date().toUTCString().toString());
    this.str_datetime = this.converter.formatDate(new Date().toUTCString());
    // this.str_datetime = new Date().toLocaleString.toString();
  }


  gotoHome() {
    this.router.navigate(['/tabs/home']);
  }

  async absen() {

    const jwt = this.session.getToken();
    const data = JSON.parse(this.session.getData());
    this.updateUser(data['kode_tps'], jwt);

  }

  async updateUser(kode_tps, jwt) {

    // loading
    const loading = await this.loadingController.create({
      message: 'Loading...',
      // duration: 2000
    });
    await loading.present();

    // variable update user
    const updateUserVal = {};
    updateUserVal['id'] = kode_tps;
    updateUserVal['latitude'] = this.str_latitude;
    updateUserVal['longitude'] = this.str_longitude;
    updateUserVal['status_absen'] = true;
    const updateUserJson = JSON.stringify(updateUserVal);
    console.log(this.updateUserUrl);
    console.log(updateUserJson);

    // call post
    this.httpRequest.httpPost(this.updateUserUrl, updateUserJson).subscribe(
      // if ok
      async result => {
        console.log(result);

        // message
        await loading.dismiss();
        const toast = await this.toastController.create({
          message: 'Absen sukses',
          duration: 2000
        });
        await toast.present();

         // set new data
         const new_data = result['result'];
         new_data['iat'] = JSON.parse(this.session.getData())['iat'];
         new_data['exp'] = JSON.parse(this.session.getData())['exp'];
         new_data['latitude'] = this.str_latitude;
         new_data['longitude'] = this.str_longitude;
         new_data['status_absen'] = true;
         this.session.setData(new_data);

        // redirect
        this.router.navigate(['/tabs/home']);
      },
      // if error
      async error => {
        console.log('error');
        console.log(error);
        await loading.dismiss();
      }
    );
  }

  async getAddress(latitude, longitude) {

    // loading
    const loading = await this.loadingController.create({
      message: 'Loading...',
      // duration: 2000
    });
    await loading.present();

    // variable update user
    const esriReverseGeocodeVal = {};
    // esriReverseGeocodeVal['f'] = 'json';
    // esriReverseGeocodeVal['location'] = longitude + ',' + latitude;
    // esriReverseGeocodeVal['token'] = '-2E5D5zhtt_YKBqhqviuLlonKyUCMexsPvwSTLFlZcOrv1LOIo8Bc4qLmzg2UyOrWGM8X7wl07sGpxKvx7tjCb0y2k2yxZX_1Dt0qAzXd1S3v0RqGKv7yaFIWtvH1-4fSK1oJLHLbjQPMfKCrT2vzw..';
    const esriReverseGeocodeJson = JSON.stringify(esriReverseGeocodeVal);
    const mapboxurl = this.esriReverseGeocodeUrl + longitude + ',' + latitude + '.json?access_token=pk.eyJ1IjoiYWthdGdlbGFyIiwiYSI6ImNqc3gxODF2NDBoMmU0YW14dzJqMXIzcDAifQ.JTOmGtdJ-V1HEvrH-XbThQ';
    console.log(mapboxurl);
    console.log(esriReverseGeocodeJson);

    // call post
    this.httpRequest.httpGet(mapboxurl, esriReverseGeocodeVal).subscribe(
      // if ok
      async result => {
        await loading.dismiss();
        console.log(result);
        this.str_address = result['features'][0]['place_name'];
      },
      // if error
      async error => {
        await loading.dismiss();
        console.log('error');
        console.log(error);
      }
    );
  }

  getLocation() {

    // get geo position
    this.geolocation.getCurrentPosition().then((resp) => {
      this.str_latitude = resp.coords.latitude.toString();
      this.str_longitude = resp.coords.longitude.toString();
      console.log(resp.coords.latitude);
      console.log(resp.coords.longitude);
      this.getAddress(this.str_latitude, this.str_longitude);
     }).catch((error) => {
       console.log('Error getting location', error);
     });

  }



}
