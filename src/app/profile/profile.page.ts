import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SessionService } from '../services/session.service';
import { ErrorMessageService } from '../services/error-message.service';
import { HttpRequestService } from '../services/http-request.service';
import { ConverterService } from '../services/converter.service';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.page.html',
  styleUrls: ['profile.page.scss']
})
export class ProfilePage {

  // updateUserUrl = 'http://us-central1-monitoring-suara-pilpres-2019.cloudfunctions.net/updateUser';
  updateUserUrl = '/api/updateUser';

  passwordForm: FormGroup;
  profileForm: FormGroup;
  str_nama = '';
  str_no_hp = '';
  str_tempat_lahir = '';
  str_tanggal_lahir = '';
  str_alamat = '';
  str_username = '';
  str_kode_tps = '';
  str_nama_provinsi = '';
  str_nama_kota = '';
  str_nama_kecamatan = '';
  str_nama_kelurahan = '';
  str_nama_tps = '';

  constructor(
    private router: Router,
    private session: SessionService,
    private httpRequest: HttpRequestService,
    private errorMessage: ErrorMessageService,
    private converter: ConverterService,
    public loadingController: LoadingController,
    public toastController: ToastController) {

      const data = JSON.parse(this.session.getData());
      this.str_nama = data['nama'];
      this.str_no_hp = data['no_hp'];
      this.str_tempat_lahir = data['tempat_lahir'];
      this.str_tanggal_lahir = data['tanggal_lahir'];
      this.str_alamat = data['alamat'];
      this.str_username = data['username'];
      this.str_kode_tps = data['kode_tps'];
      this.str_nama_provinsi = data['nama_provinsi'];
      this.str_nama_kota = data['nama_kota'];
      this.str_nama_kecamatan = data['nama_kecamatan'];
      this.str_nama_kelurahan = data['nama_kelurahan'];
      this.str_nama_tps = data['nama_tps'];

      const txt_username = new FormControl('', [Validators.required]);
      const txt_password = new FormControl('', [Validators.required]);

      this.passwordForm = new FormGroup({
        username: txt_username,
        password: txt_password
      });

      this.passwordForm.setValue({
        username: this.str_username,
        password: ''
      });

      const txt_nama = new FormControl('', [Validators.required]);
      const txt_alamat = new FormControl('', [Validators.required]);
      const txt_no_hp = new FormControl('', [Validators.required]);
      const txt_tempat_lahir = new FormControl('', [Validators.required]);
      const txt_tanggal_lahir = new FormControl('', [Validators.required]);

      this.profileForm = new FormGroup({
        nama: txt_nama,
        alamat: txt_alamat,
        no_hp: txt_no_hp,
        tempat_lahir: txt_tempat_lahir,
        tanggal_lahir: txt_tanggal_lahir
      });

      this.profileForm.setValue({
        nama: this.str_nama,
        alamat: this.str_alamat,
        no_hp: this.str_no_hp,
        tempat_lahir: this.str_tempat_lahir,
        tanggal_lahir: this.str_tanggal_lahir
      });
    }

  async updateFoto() {
    const toast = await this.toastController.create({
      color: 'success',
      position: 'top',
      message: 'Foto profil berhasil diupdate.',
      duration: 2000
    });
    toast.present();
  }

  async updatePassword() {
    // variable login user
    const passwordVal = this.passwordForm.value;
    passwordVal['id'] = JSON.parse(this.session.getData())['kode_tps'];
    const passwordJson = JSON.stringify(passwordVal);
    console.log(this.updateUserUrl);
    console.log(passwordJson);

    // loading
    const loading = await this.loadingController.create({
      message: 'Loading...',
      // duration: 2000
    });
    await loading.present();

    // call post login
    this.httpRequest.httpPost(this.updateUserUrl, passwordJson).subscribe(
      // if ok
      async result => {
        console.log(result);

        if (result['status'] === true) {

          // set new data
          const new_data = result['result'];
          new_data['iat'] = JSON.parse(this.session.getData())['iat'];
          new_data['exp'] = JSON.parse(this.session.getData())['exp'];
          this.session.setData(new_data);

          // message
          await loading.dismiss();
          const toast = await this.toastController.create({
            message: result['message'],
            duration: 2000
          });
          await toast.present();

          // redirect
          this.router.navigate(['/tabs/home']);

        } else {
          // message
          await loading.dismiss();
          const toast = await this.toastController.create({
            message: result['message'],
            duration: 2000
          });
          await toast.present();
        }
      },
      // if error
      async error => {
        console.log('error');
        console.log(error);
        // message
        await loading.dismiss();
        const toast = await this.toastController.create({
          message: error.message,
          duration: 2000
        });
        await toast.present();
      }
    );
  }

  async updateProfile() {
    // variable login user
    const profileVal = this.profileForm.value;
    profileVal['id'] = JSON.parse(this.session.getData())['kode_tps'];
    const profileJson = JSON.stringify(profileVal);
    console.log(this.updateUserUrl);
    console.log(profileJson);

    // loading
    const loading = await this.loadingController.create({
      message: 'Loading...',
      // duration: 2000
    });
    await loading.present();

    // call post login
    this.httpRequest.httpPost(this.updateUserUrl, profileJson).subscribe(
      // if ok
      async result => {
        console.log(result);

        if (result['status'] === true) {

          // set new data
          const new_data = result['result'];
          new_data['iat'] = JSON.parse(this.session.getData())['iat'];
          new_data['exp'] = JSON.parse(this.session.getData())['exp'];
          this.session.setData(new_data);

          // message
          await loading.dismiss();
          const toast = await this.toastController.create({
            message: result['message'],
            duration: 2000
          });
          await toast.present();

          // redirect
          this.router.navigate(['/tabs/home']);

        } else {
          // message
          await loading.dismiss();
          const toast = await this.toastController.create({
            message: result['message'],
            duration: 2000
          });
          await toast.present();
        }
      },
      // if error
      async error => {
        console.log('error');
        console.log(error);
        // message
        await loading.dismiss();
        const toast = await this.toastController.create({
          message: error.message,
          duration: 2000
        });
        await toast.present();
      }
    );
  }

  async logout() {
    this.session.logOut();
    const toast = await this.toastController.create({
      // color: 'success',
      // position: 'top',
      message: 'Logout sukses.',
      duration: 2000
    });
    toast.present();
    this.router.navigate(['/login']);
  }

  gotoHome() {
    console.log('home');
    this.router.navigate(['/tabs/home']);
  }
}
