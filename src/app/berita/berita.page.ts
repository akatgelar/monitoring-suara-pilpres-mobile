import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-berita',
  templateUrl: 'berita.page.html',
  styleUrls: ['berita.page.scss']
})
export class BeritaPage {

  public berita: Observable<any[]>;

  constructor(
    private router: Router,
    public db: AngularFirestore) {
      this.loadBerita();
    }

  gotoHome() {
    console.log('home');
    this.router.navigate(['/tabs/home']);
  }

  loadBerita() {
    //  get Data
    this.berita = this.db.collection('/test_berita').valueChanges();
  }

}
