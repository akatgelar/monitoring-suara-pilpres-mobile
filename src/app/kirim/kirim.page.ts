import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, takeLast } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SessionService } from '../services/session.service';
import { ErrorMessageService } from '../services/error-message.service';
import { HttpRequestService } from '../services/http-request.service';
import { ConverterService } from '../services/converter.service';
import { AngularFireStorage } from 'angularfire2/storage';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-kirim',
  templateUrl: 'kirim.page.html',
  styleUrls: ['kirim.page.scss']
})



export class KirimPage {

  // insertSuaraTPSUrl = 'http://us-central1-monitoring-suara-pilpres-2019.cloudfunctions.net/insertSuaraTPS';
  insertSuaraTPSUrl = '/api/insertSuaraTPS';
  // updateUserUrl = 'http://us-central1-monitoring-suara-pilpres-2019.cloudfunctions.net/updateUser';
  updateUserUrl = '/api/updateUser';

  kirimForm: FormGroup;

  imageSrc1: string | ArrayBuffer;
  imageSrc2: string | ArrayBuffer;
  imageSrc3: string | ArrayBuffer;
  imageLink1: string;
  imageLink2: string;
  imageLink3: string;
  uploadPercent1: Observable<number>;
  uploadPercent2: Observable<number>;
  uploadPercent3: Observable<number>;
  downloadURL1: Observable<string>;
  downloadURL2: Observable<string>;
  downloadURL3: Observable<string>;

  str_latitude = '0.0';
  str_longitude = '0.0';

  constructor(
    private router: Router,
    private session: SessionService,
    private httpRequest: HttpRequestService,
    private errorMessage: ErrorMessageService,
    private converter: ConverterService,
    private afStorage: AngularFireStorage,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public geolocation: Geolocation
    // private http: HttpClient,
    // private loadingCtrl: LoadingController,
    // private toastCtrl: ToastController
    ) {
      const txt_suara_1 = new FormControl('', [Validators.required]);
      const txt_suara_2 = new FormControl('', [Validators.required]);
      const txt_suara_sah = new FormControl('', [Validators.required]);
      const txt_suara_tidaksah = new FormControl('', [Validators.required]);
      const txt_suara_total = new FormControl('', [Validators.required]);
      const txt_suara_scan_1 = new FormControl('', [Validators.required]);
      const txt_suara_scan_2 = new FormControl('', [Validators.required]);
      const txt_suara_scan_3 = new FormControl('', [Validators.required]);

      this.kirimForm = new FormGroup({
        suara_1: txt_suara_1,
        suara_2: txt_suara_2,
        suara_total: txt_suara_sah,
        suara_sah: txt_suara_tidaksah,
        suara_tidaksah: txt_suara_total,
        suara_scan_1: txt_suara_scan_1,
        suara_scan_2: txt_suara_scan_2,
        suara_scan_3: txt_suara_scan_3
      });
      this.getLocation();
    }


  gotoHome() {
    console.log('home');
    this.router.navigate(['/tabs/home']);
  }

  async kirim() {

    this.kirimForm.patchValue({
      suara_latitude: this.imageLink2
    });

    // variable login user
    const insertSuaraTPSVal = this.kirimForm.value;
    insertSuaraTPSVal['kode_tps'] = JSON.parse(this.session.getData())['kode_tps'];
    insertSuaraTPSVal['suara_latitude'] = this.str_latitude;
    insertSuaraTPSVal['suara_longitude'] = this.str_longitude;
    const insertSuaraTPSJson = JSON.stringify(insertSuaraTPSVal);
    console.log(this.insertSuaraTPSUrl);
    console.log(insertSuaraTPSJson);

    // loading
    const loading = await this.loadingController.create({
      message: 'Loading...',
      // duration: 2000
    });
    await loading.present();

    // call post login
    this.httpRequest.httpPost(this.insertSuaraTPSUrl, insertSuaraTPSJson).subscribe(
      // if ok
      async result => {
        console.log(result);

        if (result['ok'] === true) {

          // message
          await loading.dismiss();
          const toast = await this.toastController.create({
            message: result['message'],
            duration: 2000
          });
          await toast.present();

          this.updateUser(insertSuaraTPSVal['kode_tps']);

        } else {

          // message
          await loading.dismiss();
          const toast = await this.toastController.create({
            message: result['message'],
            duration: 2000
          });
          await toast.present();

          // redirect
          this.router.navigate(['/tabs/home']);
        }
      },
      // if error
      async error => {
        console.log('error');
        console.log(error);
        // message
        await loading.dismiss();
        const toast = await this.toastController.create({
          message: error.message,
          duration: 2000
        });
        await toast.present();
      }
    );

  }

  updateUser(kode_user) {

    // variable update user
    const updateUserVal = {};
    updateUserVal['id'] = kode_user;
    updateUserVal['status_kirim'] = true;
    const updateUserJson = JSON.stringify(updateUserVal);
    console.log(this.updateUserUrl);
    console.log(updateUserJson);

    // call post
    this.httpRequest.httpPost(this.updateUserUrl, updateUserJson).subscribe(
      // if ok
      async result => {
        console.log(result);

         // set new data
         const new_data = result['result'];
         new_data['iat'] = JSON.parse(this.session.getData())['iat'];
         new_data['exp'] = JSON.parse(this.session.getData())['exp'];
         new_data['status_kirim'] = true;
         this.session.setData(new_data);

         // redirect
         this.router.navigate(['/tabs/home']);
      },
      // if error
      async error => {
        console.log('error');
        console.log(error);
      }
    );
  }

  upload1(event) {

    // declare file
    const file = event.target.files[0];
    const filePath = '/test_tps/' + JSON.parse(this.session.getData())['kode_tps'] + '_' + this.getDateTimeFormat() + '.png';
    const fileRef = this.afStorage.ref(filePath);
    const task = this.afStorage.upload(filePath, file);

    // load img to component
    const reader = new FileReader();
    reader.onload = e => this.imageSrc1 = reader.result;
    reader.readAsDataURL(file);

    // observe percentage changes
    this.uploadPercent1 = task.percentageChanges();

    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(() => {
          this.downloadURL1 = fileRef.getDownloadURL();
          this.downloadURL1.subscribe(url => {
            if (url) {
              this.imageLink1 = url;
              this.kirimForm.patchValue({
                suara_scan_1: this.imageLink1
              });
            }
          });
        })
     )
    .subscribe();
  }

  upload2(event) {

    // declare file
    const file = event.target.files[0];
    const filePath = '/test_tps/' + JSON.parse(this.session.getData())['kode_tps'] + '_' + this.getDateTimeFormat() + '.png';
    const fileRef = this.afStorage.ref(filePath);
    const task = this.afStorage.upload(filePath, file);

    // load img to component
    const reader = new FileReader();
    reader.onload = e => this.imageSrc2 = reader.result;
    reader.readAsDataURL(file);

    // observe percentage changes
    this.uploadPercent2 = task.percentageChanges();

    // get notified when the download URL is available
    task.snapshotChanges().pipe(
      finalize(() => {
        this.downloadURL2 = fileRef.getDownloadURL();
        this.downloadURL2.subscribe(url => {
          if (url) {
            this.imageLink2 = url;
            this.kirimForm.patchValue({
              suara_scan_2: this.imageLink2
            });
          }
        });
      })
     )
    .subscribe();
  }

  upload3(event) {

    // declare file
    const file = event.target.files[0];
    const filePath = '/test_tps/' + JSON.parse(this.session.getData())['kode_tps'] + '_' + this.getDateTimeFormat() + '.png';
    const fileRef = this.afStorage.ref(filePath);
    const task = this.afStorage.upload(filePath, file);

    // load img to component
    const reader = new FileReader();
    reader.onload = e => this.imageSrc3 = reader.result;
    reader.readAsDataURL(file);

    // observe percentage changes
    this.uploadPercent3 = task.percentageChanges();

    // get notified when the download URL is available
    task.snapshotChanges().pipe(
      finalize(() => {
        this.downloadURL3 = fileRef.getDownloadURL();
        this.downloadURL3.subscribe(url => {
          if (url) {
            this.imageLink3 = url;
            this.kirimForm.patchValue({
              suara_scan_3: this.imageLink3
            });
          }
        });
      })
     )
    .subscribe();
  }


  getLocation() {

    // get geo position
    this.geolocation.getCurrentPosition().then((resp) => {
      this.str_latitude = resp.coords.latitude.toString();
      this.str_longitude = resp.coords.longitude.toString();
      console.log(resp.coords.latitude);
      console.log(resp.coords.longitude);
     }).catch((error) => {
       console.log('Error getting location', error);
     });

  }

  getDateTimeFormat() {
    const date = new Date();
    let year = '';
    let month = '';
    let day = '';
    let hour = '';
    let minute = '';
    let second = '';

    year = (date.getFullYear()).toString();

    if ((date.getMonth() + 1) < 10) {
      month = '0' + (date.getMonth() + 1);
    } else {
      month = (date.getMonth() + 1).toString();
    }

    if (date.getDate() < 10) {
      day = '0' + date.getDate();
    } else {
      day = date.getDate().toString();
    }

    if (date.getHours() < 10) {
      hour = '0' + date.getHours();
    } else {
      hour = date.getHours().toString();
    }

    if (date.getMinutes() < 10) {
      minute = '0' + date.getMinutes();
    } else {
      minute = date.getMinutes().toString();
    }

    if (date.getSeconds() < 10) {
      second = '0' + date.getSeconds();
    } else {
      second = date.getSeconds().toString();
    }
    const date_str = year + month + day + '_' + hour + minute + second;

    return date_str;
  }



}
