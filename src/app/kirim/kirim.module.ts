import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// import { KirimPageRoutingModule } from './kirim.router.module';

import { KirimPage } from './kirim.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    // KirimPageRoutingModule
  ],
  // declarations: [KirimPage]
})
export class KirimPageModule {}
